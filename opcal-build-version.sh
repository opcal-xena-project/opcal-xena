#!/bin/bash
## this script depends on xmlstarlet
set -e

SCRIPT=`readlink -f "${BASH_SOURCE:-$0}"`

DIR_PATH=`dirname ${SCRIPT}`

VERSION=$(grep "opcal-commons-build.version" ${DIR_PATH}/dependencies.properties|cut -d'=' -f2)

echo "opcal build version is [${VERSION}]"
# update xyz.opcal.build version
xmlstarlet edit -P -L -O -u "/_:project/_:parent/_:version" -v ${VERSION} ${DIR_PATH}/pom.xml

${DIR_PATH}/mvnw -U clean compile >> /dev/null 2>&1