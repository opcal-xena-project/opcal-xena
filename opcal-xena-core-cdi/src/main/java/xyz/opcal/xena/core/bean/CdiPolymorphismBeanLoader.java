/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.core.bean;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import jakarta.enterprise.inject.spi.Bean;
import jakarta.enterprise.inject.spi.BeanManager;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import xyz.opcal.xena.core.annotation.Polymorphism;

/**
 * @author gissily
 *
 */
@Singleton
public class CdiPolymorphismBeanLoader implements PolymorphismBeanLoader {

	private BeanManager beanManager;

	@Inject
	public CdiPolymorphismBeanLoader(BeanManager beanManager) {
		this.beanManager = beanManager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getImplementsInstance(Class<T> interfaceClazz, String selector) {
		return beanManager.getBeans(interfaceClazz).stream().map(bean -> (Bean<T>) bean).filter(bean -> matchBean(bean, selector)).map(this::getInstance)
				.findFirst().orElse(null);
	}

	private <T> T getInstance(Bean<T> bean) {
		return beanManager.getContext(Singleton.class).get(bean, beanManager.createCreationalContext(bean));
	}

	private <T> boolean matchBean(Bean<T> bean, String selector) {
		Polymorphism polymorphism = getPolymorphism(bean);
		if (Objects.isNull(polymorphism)) {
			return false;
		}
		return StringUtils.equals(selector, polymorphism.selector());
	}

	private <T> Polymorphism getPolymorphism(Bean<T> bean) {
		return bean.getBeanClass().getAnnotation(Polymorphism.class);
	}

}
