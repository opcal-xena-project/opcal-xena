/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.core.context.impl;

import java.lang.annotation.Annotation;
import java.util.Set;
import java.util.stream.Collectors;

import jakarta.enterprise.inject.spi.Bean;
import jakarta.enterprise.inject.spi.BeanManager;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import lombok.Getter;
import xyz.opcal.xena.core.context.CdiBeanXenaContext;

/**
 * @author gissily
 *
 */
@Singleton
public class CdiXenaContext implements CdiBeanXenaContext {

	@Getter
	private BeanManager beanManager;

	@Inject
	public CdiXenaContext(BeanManager beanManager) {
		this.beanManager = beanManager;
	}

	@Override
	public Object getBean(String beanName) {
		return getScopeBean(beanName, Singleton.class);
	}

	@Override
	public <T> T getBean(Class<T> beanClass) {
		return getScopeBean(beanClass, Singleton.class);
	}

	@Override
	public <T> T getBean(String beanName, Class<T> beanClass) {
		return getScopeBean(beanName, beanClass, Singleton.class);
	}

	@Override
	public <T> Set<T> getBeans(Class<T> beanClass) {
		return getScopeBeans(beanClass, Singleton.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object getScopeBean(String beanName, Class<? extends Annotation> scope) {

		return beanManager.getBeans(beanName).stream() //
				.map(bean -> (Bean<Object>) bean).map(bean -> beanManager.getContext(scope).get(bean, beanManager.createCreationalContext(bean))).findFirst()
				.orElse(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getScopeBean(Class<T> beanClass, Class<? extends Annotation> scope) {
		return beanManager.getBeans(beanClass).stream() //
				.map(bean -> (Bean<T>) bean).map(bean -> beanManager.getContext(scope).get(bean, beanManager.createCreationalContext(bean))).findFirst()
				.orElse(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getScopeBean(String beanName, Class<T> beanClass, Class<? extends Annotation> scope) {
		return beanManager.getBeans(beanName).stream() //
				.filter(bean -> bean.getTypes().contains(beanClass)) //
				.map(bean -> (Bean<T>) bean).map(bean -> beanManager.getContext(scope).get(bean, beanManager.createCreationalContext(bean))).findFirst()
				.orElse(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> Set<T> getScopeBeans(Class<T> beanClass, Class<? extends Annotation> scope) {
		return beanManager.getBeans(beanClass).stream().map(bean -> (Bean<T>) bean)
				.map(bean -> beanManager.getContext(scope).get(bean, beanManager.createCreationalContext(bean))).collect(Collectors.toSet());
	}

}
