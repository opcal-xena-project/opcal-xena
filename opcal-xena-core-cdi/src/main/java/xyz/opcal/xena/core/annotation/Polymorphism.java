/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that an annotated class as polymorphic implement for one or more interfaces.
 * 
 * <p>
 * The classes' instances bean will be dynamically loaded by "PolymorphismBeanLoader", if
 * a interface need to get a bean dynamically by specific bean value.
 * 
 * 
 * @author gissily
 * @since 1.0.0
 * @see xyz.opcal.xena.core.bean.PolymorphismBeanLoader
 *
 */
@Target(value = ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Polymorphism {

	/**
	 * Interface classes that need to be polymorphic implemented.
	 * 
	 * @return a interface class array that would be polymorphic implemented
	 */
	Class<?>[] interfaceClass();

	/**
	 * 
	 * @return it is a not bean name, but a simple value for bean name
	 */
	String selector();
}
