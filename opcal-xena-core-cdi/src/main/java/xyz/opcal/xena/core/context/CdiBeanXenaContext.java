/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.core.context;

import java.lang.annotation.Annotation;
import java.util.Set;

import jakarta.enterprise.inject.spi.BeanManager;

/**
 * @author gissily
 *
 */
public interface CdiBeanXenaContext extends XenaContext {

	BeanManager getBeanManager();

	Object getScopeBean(String beanName, Class<? extends Annotation> scope);

	<T> T getScopeBean(Class<T> beanClass, Class<? extends Annotation> scope);

	<T> T getScopeBean(String beanName, Class<T> beanClass, Class<? extends Annotation> scope);

	<T> Set<T> getScopeBeans(Class<T> beanClass, Class<? extends Annotation> scope);
}
