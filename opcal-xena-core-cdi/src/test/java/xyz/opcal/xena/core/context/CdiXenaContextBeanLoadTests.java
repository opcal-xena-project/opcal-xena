/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.core.context;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import xyz.opcal.test.xena.core.processors.Greeting;
import xyz.opcal.xena.core.context.impl.CdiXenaContext;

/**
 * @author gissily
 *
 */
class CdiXenaContextBeanLoadTests {

	@Test
	void getBean() {
		try (SeContainer container = SeContainerInitializer.newInstance().initialize()) {
			CdiXenaContext cdiXenaContext = container.select(CdiXenaContext.class).get();
			assertNotNull(cdiXenaContext.getBeanManager());
			assertNotNull(cdiXenaContext.getBean("DeGreeting"));
			assertNotNull(cdiXenaContext.getBean("DeGreeting", Greeting.class));
			assertNotNull(cdiXenaContext.getBeans(Greeting.class));
			assertNotNull(cdiXenaContext.getBean(XenaContext.class));
			assertFalse(cdiXenaContext.getBeans(Greeting.class).isEmpty());
		}
	}

}
