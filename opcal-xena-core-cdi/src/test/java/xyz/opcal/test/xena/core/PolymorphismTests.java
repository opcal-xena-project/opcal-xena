/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.test.xena.core;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import xyz.opcal.test.xena.core.service.GreetingService;

/**
 * @author gissily
 *
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PolymorphismTests {

	@Test
	@Order(0)
	void greetingTest() {
		try (SeContainer container = SeContainerInitializer.newInstance().initialize()) {
			GreetingService greetingService = container.select(GreetingService.class).get();
			assertNotNull(greetingService.sayHello("en"));
		}
	}

	@Test
	@Order(1)
	void greetingError() {
		try (SeContainer container = SeContainerInitializer.newInstance().initialize()) {
			GreetingService greetingService = container.select(GreetingService.class).get();
			assertThrows(IllegalArgumentException.class, () -> greetingService.sayHello("ee"));
		}
	}

}
