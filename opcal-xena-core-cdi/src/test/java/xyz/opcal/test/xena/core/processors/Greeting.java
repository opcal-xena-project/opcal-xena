package xyz.opcal.test.xena.core.processors;

public interface Greeting {

	String sayHello();
}
