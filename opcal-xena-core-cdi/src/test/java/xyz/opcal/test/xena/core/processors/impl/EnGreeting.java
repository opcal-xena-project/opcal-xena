package xyz.opcal.test.xena.core.processors.impl;

import xyz.opcal.test.xena.core.processors.Greeting;
import xyz.opcal.xena.core.annotation.Polymorphism;

@Polymorphism(selector = "en", interfaceClass = Greeting.class)
public class EnGreeting implements Greeting {

	@Override
	public String sayHello() {
		return "hello";
	}

}
