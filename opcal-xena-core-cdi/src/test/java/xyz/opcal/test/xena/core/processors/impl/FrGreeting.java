package xyz.opcal.test.xena.core.processors.impl;

import xyz.opcal.test.xena.core.processors.Greeting;
import xyz.opcal.xena.core.annotation.Polymorphism;

@Polymorphism(selector = "fr", interfaceClass = Greeting.class)
public class FrGreeting implements Greeting {

	@Override
	public String sayHello() {
		return "Bonjour";
	}

}
