/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.common.service.payment.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

import xyz.opcal.xena.common.dto.PaymentParam;
import xyz.opcal.xena.common.service.payment.IPaymentHandler;
import xyz.opcal.xena.core.annotation.Polymorphism;
import xyz.opcal.xena.core.utils.JsonUtils;

/**
 * Simulation for visa payment parameters generating
 */
@Polymorphism(selector = "visa", interfaceClass = IPaymentHandler.class)
public class VisaPaymentHandler implements IPaymentHandler {

	private static final String TOKEN = "12821*daau#eueu";

	@Override
	public Map<String, Object> generatePayment(PaymentParam param) {
		Map<String, Object> data = new HashMap<>();
		data.put("total", param.getTotal());
		data.put("signature", DigestUtils.sha1Hex(JsonUtils.toJson(param) + TOKEN));
		return data;
	}

}
