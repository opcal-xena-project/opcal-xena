/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.opcal.xena.common.dto.PaymentData;
import xyz.opcal.xena.common.dto.PaymentRequest;
import xyz.opcal.xena.common.service.PaymentService;
import xyz.opcal.xena.common.service.payment.IPaymentHandler;
import xyz.opcal.xena.core.bean.PolymorphismBeanLoader;

@Service
public class PaymentServiceImpl implements PaymentService {

	private @Autowired PolymorphismBeanLoader polymorphismBeanLoader;

	@Override
	public PaymentData generate(PaymentRequest request) {

		if (request == null) {
			throw new IllegalArgumentException("payment request can not be null");
		}

		IPaymentHandler paymentHandler = polymorphismBeanLoader.getImplementsInstance(IPaymentHandler.class, request.getPaymentMethod());

		if (paymentHandler == null) {
			throw new IllegalArgumentException("not support payment method " + request.getPaymentMethod());
		}

		PaymentData data = new PaymentData();
		data.setData(paymentHandler.generatePayment(request.getParam()));
		data.setPaymentMethod(request.getPaymentMethod());
		return data;
	}

}
