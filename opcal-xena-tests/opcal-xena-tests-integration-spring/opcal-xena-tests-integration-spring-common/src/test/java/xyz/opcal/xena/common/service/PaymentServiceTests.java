/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.common.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import xyz.opcal.xena.common.CommonConfiguration;
import xyz.opcal.xena.common.dto.PaymentData;
import xyz.opcal.xena.common.dto.PaymentParam;
import xyz.opcal.xena.common.dto.PaymentRequest;
import xyz.opcal.xena.core.annotation.EnableXena;

@EnableXena
@SpringJUnitConfig(classes = CommonConfiguration.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PaymentServiceTests {

	private @Autowired PaymentService paymentService;

	@Test
	@Order(1)
	void validateException() {
		assertThrows(IllegalArgumentException.class, () -> paymentService.generate(null));
		PaymentRequest paymentRequest = new PaymentRequest();
		assertThrows(IllegalArgumentException.class, () -> paymentService.generate(paymentRequest));
	}

	@Test
	@Order(2)
	void payment() {
		PaymentParam paymentParam = new PaymentParam();
		paymentParam.setTotal(new BigDecimal("123456789.98"));
		PaymentRequest visaRequest = new PaymentRequest();
		visaRequest.setPaymentMethod("visa");
		visaRequest.setParam(paymentParam);

		PaymentData visaData = paymentService.generate(visaRequest);
		assertNotNull(visaData);
		assertNotNull(visaData.getData());
		assertEquals(visaRequest.getPaymentMethod(), visaData.getPaymentMethod());

		PaymentRequest paypalRequest = new PaymentRequest();
		paypalRequest.setPaymentMethod("paypal");
		paypalRequest.setParam(paymentParam);

		PaymentData paypalData = paymentService.generate(paypalRequest);
		assertNotNull(paypalData);
		assertNotNull(paypalData.getData());
		assertEquals(paypalData.getPaymentMethod(), paypalData.getPaymentMethod());
	}

	@Test
	@Order(3)
	void notSupport() {
		PaymentParam paymentParam = new PaymentParam();
		paymentParam.setTotal(new BigDecimal("123456789.98"));
		PaymentRequest mastercardRequest = new PaymentRequest();
		mastercardRequest.setPaymentMethod("mastercard");
		mastercardRequest.setParam(paymentParam);

		assertThrows(IllegalArgumentException.class, () -> paymentService.generate(mastercardRequest));
	}
}
