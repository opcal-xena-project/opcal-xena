/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.web.service.impl;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.opcal.xena.common.dto.PaymentData;
import xyz.opcal.xena.common.dto.PaymentParam;
import xyz.opcal.xena.common.dto.PaymentRequest;
import xyz.opcal.xena.common.service.PaymentService;
import xyz.opcal.xena.web.service.OrderAssistanceService;

/**
 * @author gissily
 *
 */
@Service
public class OrderAssistanceServiceImpl implements OrderAssistanceService {

	private @Autowired PaymentService paymentService;

	@Override
	public PaymentData getPaymentData(BigDecimal total, String payment) {
		PaymentParam paymentParam = new PaymentParam();
		paymentParam.setTotal(new BigDecimal("123456789.98"));
		PaymentRequest paymentRequest = new PaymentRequest();
		paymentRequest.setPaymentMethod("visa");
		paymentRequest.setParam(paymentParam);
		return paymentService.generate(paymentRequest);
	}

}
