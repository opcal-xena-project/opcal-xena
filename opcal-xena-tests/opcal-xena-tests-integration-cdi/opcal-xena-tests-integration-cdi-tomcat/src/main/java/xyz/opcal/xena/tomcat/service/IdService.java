/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.tomcat.service;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import xyz.opcal.xena.common.sequence.SequenceGenerator;
import xyz.opcal.xena.common.sequence.SequenceType;

/**
 * @author gissily
 *
 */
@Path("/id")
public class IdService {

	@Inject
	private SequenceGenerator sequenceGenerator;

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String id(@QueryParam("type") String type) {
		return sequenceGenerator.genrtateId(SequenceType.valueOf(type));
	}
}
