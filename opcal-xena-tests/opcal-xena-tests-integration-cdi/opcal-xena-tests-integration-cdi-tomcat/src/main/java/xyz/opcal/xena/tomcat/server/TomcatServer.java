/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.tomcat.server;

import java.io.File;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.jboss.weld.environment.servlet.Listener;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gissily
 *
 */
@Slf4j
public class TomcatServer {

	private final int port;
	private Tomcat tomcat;

	public TomcatServer(int port) {
		this.port = port;
	}

	public void start() throws Exception {
		tomcat = new Tomcat();
		tomcat.setPort(port);
		Context ctx = tomcat.addContext("/", new File("src/main/resources").getAbsolutePath());

		Tomcat.addServlet(ctx, "jersey-container-servlet", new ServletContainer(new ResourceConfig().packages("xyz.opcal.xena.tomcat")));
		ctx.addServletMappingDecoded("/*", "jersey-container-servlet");

		ctx.addApplicationListener(Listener.class.getName());
		tomcat.getConnector();
		tomcat.start();
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			try {
				tomcat.stop();
				log.info("shutdown by hook");
			} catch (Exception e) {
				log.error("stop error: ", e);
			}
		}));
	}

	public void stop() throws Exception {
		tomcat.stop();
	}
}
