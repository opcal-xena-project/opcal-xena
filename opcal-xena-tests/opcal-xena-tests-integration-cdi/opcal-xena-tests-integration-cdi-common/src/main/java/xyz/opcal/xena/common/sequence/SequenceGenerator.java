/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.common.sequence;

import jakarta.inject.Inject;
import xyz.opcal.xena.core.bean.PolymorphismBeanLoader;

public class SequenceGenerator {

	private PolymorphismBeanLoader polymorphismBeanLoader;

	@Inject
	public SequenceGenerator(PolymorphismBeanLoader polymorphismBeanLoader) {
		this.polymorphismBeanLoader = polymorphismBeanLoader;
	}

	public String genrtateId(SequenceType sequenceType) {

		IdGenerator generator = polymorphismBeanLoader.getImplementsInstance(IdGenerator.class, sequenceType.getName());
		if (generator == null) {
			throw new IllegalStateException("not support sequence type");
		}
		return generator.generate();
	}
}
