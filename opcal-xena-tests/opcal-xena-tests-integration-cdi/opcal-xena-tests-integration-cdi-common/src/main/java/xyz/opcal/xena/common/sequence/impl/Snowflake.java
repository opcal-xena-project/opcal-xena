/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.common.sequence.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;

/**
 * java edition of Twitter <b>Snowflake</b>, a network service for generating
 * unique ID numbers at high scale with some simple guarantees.
 * 
 * https://github.com/twitter/snowflake
 */
public class Snowflake {

	private static final long UNUSED_BITS = 1L;
	private static final long TIMESTAMP_BITS = 41L;
	private static final long DATACENTER_ID_BITS = 5L;
	private static final long WORKER_ID_BITS = 5L;
	private static final long SEQUENCE_BITS = 12L;

	/**
	 * 2020-01-01
	 */
	private static final long DEFAULT_EPOCH = 1577836800000L;

	private static final long MAX_DATACENTER_ID = -1L ^ (-1L << DATACENTER_ID_BITS); // 2^5-1
	private static final long MAX_WORKER_ID = -1L ^ (-1L << WORKER_ID_BITS); // 2^5-1
	private static final long MAX_SEQUENCE = -1L ^ (-1L << SEQUENCE_BITS); // 2^12-1

	private static final long TIMESTAMP_OFFSET = UNUSED_BITS;
	private static final long DATACENTER_ID_OFFSET = UNUSED_BITS + TIMESTAMP_BITS;
	private static final long WORKER_ID_OFFSET = UNUSED_BITS + TIMESTAMP_BITS + DATACENTER_ID_BITS;
	private static final long SEQUENCE_OFFSET = UNUSED_BITS + TIMESTAMP_BITS + DATACENTER_ID_BITS + WORKER_ID_BITS;

	private static final long TIMESTAMP_SHIFT = DATACENTER_ID_BITS + WORKER_ID_BITS + SEQUENCE_BITS;
	private static final long DATACENTER_ID_SHIFT = WORKER_ID_BITS + SEQUENCE_BITS;
	private static final long WORKER_ID_SHIFT = SEQUENCE_BITS;

	private static final String SPACE_CHAR = " ";
	private static final String ZERO_CHAR = "0";

	@Getter
	private final long epoch;
	private final long datacenterId;
	private final long workerId;

	private volatile long sequence = 0L;
	private volatile long lastTimestamp = -1L;

	public Snowflake(long datacenterId, long workerId, long customEpoch) {
		if (datacenterId > MAX_DATACENTER_ID || datacenterId < 0) {
			throw new IllegalArgumentException(String.format("datacenter Id can't be greater than %d or less than 0", MAX_DATACENTER_ID));
		}
		if (workerId > MAX_WORKER_ID || workerId < 0) {
			throw new IllegalArgumentException(String.format("worker Id can't be greater than %d or less than 0", MAX_WORKER_ID));
		}

		this.datacenterId = datacenterId;
		this.workerId = workerId;
		this.epoch = customEpoch;
	}

	public Snowflake(long datacenterId, long workerId) {
		this(datacenterId, workerId, DEFAULT_EPOCH);
	}

	public synchronized long nextId() {
		long currTimestamp = timestamp();

		if (currTimestamp < lastTimestamp) {
			throw new IllegalStateException(String.format("Clock moved backwards. Refusing to generate id for %d milliseconds", lastTimestamp - currTimestamp));
		}
		if (currTimestamp == lastTimestamp) {
			sequence = (sequence + 1) & MAX_SEQUENCE;
			if (sequence == 0) {
				currTimestamp = waitNextMillis(currTimestamp);
			}
		} else {
			sequence = 0L;
		}

		lastTimestamp = currTimestamp;

		return ((currTimestamp - this.epoch) << TIMESTAMP_SHIFT) //
				| (datacenterId << DATACENTER_ID_SHIFT) //
				| (workerId << WORKER_ID_SHIFT) //
				| sequence;
	}

	protected long waitNextMillis(long currTimestamp) {
		while (currTimestamp <= lastTimestamp) {
			currTimestamp = timestamp();
		}
		return currTimestamp;
	}

	protected long timestamp() {
		return System.currentTimeMillis();
	}

	@Override
	public String toString() {
		return "Snowflake  [timestampBits=" + TIMESTAMP_BITS + ", datacenterIdBits=" + DATACENTER_ID_BITS + ", workerIdBits=" + WORKER_ID_BITS
				+ ", sequenceBits=" + SEQUENCE_BITS + ", epoch=" + epoch + ", datacenterId=" + datacenterId + ", workerId=" + workerId + ", sequence="
				+ sequence + ", lastTimestamp=" + lastTimestamp + "]";
	}

	/**
	 * <pre>
	 * <table border="1" style="text-align: center;">
	 * <thead>
	 * <tr>
	 * <th>Unused</th>
	 * <th>Timestamp</th>
	 * <th>DatacenterId</th>
	 * <th>WorkId</th>
	 * <th>Sequence</th>
	 * </tr>
	 * <thead> <tbody>
	 * <tr>
	 * <td>1</td>
	 * <td>41</td>
	 * <td>5</td>
	 * <td>5</td>
	 * <td>12</td>
	 * </tr>
	 * <tr>
	 * <td>0</td>
	 * <td>00000000000000000000000000000000000000000</td>
	 * <td>00000</td>
	 * <td>00000</td>
	 * <td>000000000000</td>
	 * </tr>
	 * </tbody>
	 * </table>
	 * </pre>
	 * 
	 * 
	 * @param id 
	 * @return
	 */
	public long[] parseId(long id) {

		long timestamp = trim(id, TIMESTAMP_OFFSET, TIMESTAMP_SHIFT) + this.epoch;
		long dId = trim(id, DATACENTER_ID_OFFSET, DATACENTER_ID_SHIFT);
		long wId = trim(id, WORKER_ID_OFFSET, WORKER_ID_SHIFT);
		long seq = trim(id, SEQUENCE_OFFSET, 0);

		return new long[] { timestamp, dId, wId, seq };
	}

	private long trim(long id, long offset, long length) {
		return id << offset >> offset >> length;
	}

	public String formatId(long id) {
		long[] arr = parseId(id);
		String tmf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date(arr[0]));
		return String.format("%s @(%d,%d) #%d", tmf, arr[1], arr[2], arr[3]);
	}

	public static String toBinaryString(long id) {
		return String.format("%64s", Long.toBinaryString(id)).replace(SPACE_CHAR, ZERO_CHAR);
	}

	public static String toHexString(long id) {
		return String.format("%16s", Long.toHexString(id)).replace(SPACE_CHAR, ZERO_CHAR);
	}

}