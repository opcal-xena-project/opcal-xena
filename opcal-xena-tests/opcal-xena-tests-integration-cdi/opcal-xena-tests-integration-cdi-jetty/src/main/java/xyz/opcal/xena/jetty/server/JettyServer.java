/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.jetty.server;

import org.eclipse.jetty.cdi.CdiDecoratingListener;
import org.eclipse.jetty.cdi.CdiServletContainerInitializer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.webapp.WebAppContext;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gissily
 *
 */
@Slf4j
public class JettyServer {

	private final int port;
	private Server server;

	/**
	 * 
	 */
	public JettyServer(int port) {
		this.port = port;
	}

	public void start() throws Exception {
		server = new Server(port);
		WebAppContext context = new WebAppContext();

		context.setContextPath("/");
		context.setResourceBase("src/main/resources");
		context.getMimeTypes().addMimeMapping("txt", "text/plain;charset=utf-8");
		context.setInitParameter(CdiServletContainerInitializer.CDI_INTEGRATION_ATTRIBUTE, CdiDecoratingListener.MODE);
		context.addServletContainerInitializer(new CdiServletContainerInitializer());
		context.addServletContainerInitializer(new org.jboss.weld.environment.servlet.EnhancedListener());

		context.addServlet(new ServletHolder(new ServletContainer(new ResourceConfig().packages("xyz.opcal.xena.jetty"))), "/*");

		server.setHandler(context);

		server.start();

		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			try {
				server.stop();
				log.info("shutdown by hook");
			} catch (Exception e) {
				log.error("stop error: ", e);
			}
		}));

	}

	public void stop() throws Exception {
		server.stop();
	}
}
