/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.jetty.servlet;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.xena.jetty.server.JettyServer;

/**
 * @author gissily
 *
 */
@Slf4j
@TestInstance(Lifecycle.PER_CLASS)
class JettyIdServletTest {

	JettyServer server;
	HttpClient httpClient;

	@BeforeAll
	void init() throws Exception {
		server = new JettyServer(8080);
		server.start();
		httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_1_1).connectTimeout(Duration.ofSeconds(10)).build();
	}

	@AfterAll
	void stop() throws Exception {
		server.stop();
	}

	@Test
	void test() throws IOException, InterruptedException {
		HttpRequest request1 = HttpRequest.newBuilder().GET().uri(URI.create("http://localhost:8080/id?type=UUID")).build();
		HttpResponse<String> response1 = httpClient.send(request1, HttpResponse.BodyHandlers.ofString());
		assertEquals(200, response1.statusCode());
		log.info("UUID: {}", StringUtils.trim(response1.body()));

		HttpRequest request2 = HttpRequest.newBuilder().GET().uri(URI.create("http://localhost:8080/id?type=SNOWFLAKE")).build();
		HttpResponse<String> response2 = httpClient.send(request2, HttpResponse.BodyHandlers.ofString());
		assertEquals(200, response2.statusCode());
		log.info("SNOWFLAKE: {}", StringUtils.trim(response2.body()));
	}

}
