# opcal-xena  
[![pipeline status](https://gitlab.com/opcal-xena-project/opcal-xena/badges/main/pipeline.svg)](https://gitlab.com/opcal-xena-project/opcal-xena/-/commits/main)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=opcal-xena-project_opcal-xena&metric=alert_status)](https://sonarcloud.io/dashboard?id=opcal-xena-project_opcal-xena) 
[![codecov](https://codecov.io/gl/opcal-xena-project/opcal-xena/branch/main/graph/badge.svg?token=WYIGQ66M0Q)](https://codecov.io/gl/opcal-xena-project/opcal-xena)


## Release Train Table
|  Release  |   Branch  | Spring Boot |  CDI API |
|   :---:   |   :---:   |    :---:    |   :---:  |
|  2.7.0    |    main   |   2.7.3     |   2.0.1  |
|  1.26.4   |   1.26.x  |   2.6.8     |          |


## Usage
### xena-spring-boot-starter

dependencies

```xml
<dependencies>
...
	<dependency>
	  <groupId>xyz.opcal.xena</groupId>
	  <artifactId>opcal-xena-spring-boot-starter</artifactId>
	  <version>{opcal-xena.version}</version>
	</dependency>
...
</dependencies>

```