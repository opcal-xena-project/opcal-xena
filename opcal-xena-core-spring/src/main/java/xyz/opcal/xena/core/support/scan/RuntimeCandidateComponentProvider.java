/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.support.scan;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.core.type.AnnotationMetadata;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.xena.core.support.scan.filter.RuntimeTypeFilter;

@Slf4j
public class RuntimeCandidateComponentProvider {

	private final List<RuntimeTypeFilter> includeFilters = new ArrayList<>();

	private final List<RuntimeTypeFilter> excludeFilters = new ArrayList<>();

	private ApplicationContext applicationContext;

	public RuntimeCandidateComponentProvider(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public void addIncludeFilter(RuntimeTypeFilter includeFilter) {
		this.includeFilters.add(includeFilter);
	}

	public void addExcludeFilter(RuntimeTypeFilter excludeFilter) {
		this.excludeFilters.add(0, excludeFilter);
	}

	// @formatter:off
	public Set<BeanDefinition> findCandidateComponents() {
		if (applicationContext instanceof BeanDefinitionRegistry) {
			BeanDefinitionRegistry registry = (BeanDefinitionRegistry) applicationContext;
			return Arrays.stream(applicationContext.getBeanDefinitionNames())
					.map(registry::getBeanDefinition)
					.filter(this::validateBean)
					.collect(Collectors.toSet());
		}
		return Collections.emptySet();
	}
	// @formatter:on

	boolean validateBean(BeanDefinition beanDefinition) {
		return tryGetBeanClass(beanDefinition).filter(Objects::nonNull).map(c -> isCandidateComponent(c, AnnotationMetadata.introspect(c))).orElse(false);
	}

	private Optional<Class<?>> tryGetBeanClass(BeanDefinition beanDefinition) {
		if (beanDefinition instanceof AbstractBeanDefinition) {
			return Optional.of(((AbstractBeanDefinition) beanDefinition)).filter(db -> Objects.nonNull(db.getBeanClassName())).map(db -> {
				try {
					return db.getBeanClass();
				} catch (Exception e) {
					log.debug("class [{}] bean definition  get bean class error: ", beanDefinition.getBeanClassName(), e);
				}
				return null;
			});
		} else {
			return Optional.of(applicationContext.getBeanProvider(beanDefinition.getResolvableType())).filter(Objects::nonNull).map(objectProvider -> {
				try {
					return objectProvider.getIfAvailable();
				} catch (Exception e) {
					log.debug("class [{}] objectProvider get bean error: ", beanDefinition.getBeanClassName(), e);
				}
				return null;
			}).filter(Objects::nonNull).map(Object::getClass);
		}
	}

	protected boolean isCandidateComponent(Class<?> clazz, AnnotationMetadata annotationMetadata) {
		for (RuntimeTypeFilter tf : this.excludeFilters) {
			if (tf.match(clazz, annotationMetadata)) {
				return false;
			}
		}
		for (RuntimeTypeFilter tf : this.includeFilters) {
			if (tf.match(clazz, annotationMetadata)) {
				return true;
			}
		}
		return false;
	}

}
