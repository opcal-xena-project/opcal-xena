/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.support.scan.filter;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.lang.Nullable;

public abstract class AbstractRuntimeHierarchyFilter implements RuntimeTypeFilter {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	private final boolean considerInherited;

	private final boolean considerInterfaces;

	protected AbstractRuntimeHierarchyFilter(boolean considerInherited, boolean considerInterfaces) {
		this.considerInherited = considerInherited;
		this.considerInterfaces = considerInterfaces;
	}

	@Override
	public boolean match(Class<?> clazz, AnnotationMetadata annotationMetadata) {

		if (matchSelf(clazz, annotationMetadata)) {
			return true;
		}

		if (matchClassName(annotationMetadata.getClassName())) {
			return true;
		}
		if (this.considerInherited) {
			String superClassName = annotationMetadata.getSuperClassName();
			if (superClassName != null) {
				Boolean superClassMatch = matchSuperClass(superClassName);
				if (superClassMatch != null) {
					if (superClassMatch.booleanValue()) {
						return true;
					}
				} else {
					try {
						if (match(clazz.getSuperclass(), AnnotationMetadata.introspect(clazz.getSuperclass()))) {
							return true;
						}
					} catch (Exception ex) {
						if (logger.isDebugEnabled()) {
							logger.debug("Could not read super class [{}] of type-filtered class [{}]", annotationMetadata.getSuperClassName(),
									annotationMetadata.getClassName());
						}
					}
				}
			}
		}

		if (this.considerInterfaces) {
			for (String ifc : annotationMetadata.getInterfaceNames()) {
				Boolean interfaceMatch = matchInterface(ifc);
				if (interfaceMatch != null) {
					if (interfaceMatch.booleanValue()) {
						return true;
					}
				} else {
					try {
						if (getInterfaceClass(ifc, clazz).map(c -> match(c, AnnotationMetadata.introspect(clazz))).orElse(false)) {
							return true;
						}
					} catch (Exception ex) {
						if (logger.isDebugEnabled()) {
							logger.debug("Could not read interface [{}] for type-filtered class [{}]", ifc, annotationMetadata.getClassName());
						}
					}
				}
			}
		}
		return false;
	}

	protected Optional<Class<?>> getInterfaceClass(String interfaceName, Class<?> currentClass) {
		if (Objects.nonNull(currentClass.getInterfaces())) {
			return Arrays.stream(currentClass.getInterfaces()).filter(ifc -> StringUtils.equals(interfaceName, ifc.getName())).findFirst();
		}
		return Optional.empty();
	}

	protected boolean matchSelf(Class<?> clazz, AnnotationMetadata annotationMetadata) {
		return false;
	}

	protected boolean matchClassName(String className) {
		return false;
	}

	@Nullable
	protected Boolean matchSuperClass(String superClassName) {
		return null;
	}

	@Nullable
	protected Boolean matchInterface(String interfaceName) {
		return null;
	}

}
