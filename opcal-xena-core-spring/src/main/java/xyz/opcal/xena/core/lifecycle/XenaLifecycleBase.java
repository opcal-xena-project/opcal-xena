/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.lifecycle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xyz.opcal.xena.core.lifecycle.listener.XenaLifecycleEvent;
import xyz.opcal.xena.core.lifecycle.listener.XenaLifecycleListener;

/**
 * Abstract implementation of the {@link xyz.opcal.xena.core.lifecycle.XenaLifecycle}
 * 
 * @author gissily
 *
 */
public abstract class XenaLifecycleBase implements XenaLifecycle {

	protected final Logger log = LoggerFactory.getLogger(getClass());

	private final List<XenaLifecycleListener> lifecycleListeners = new ArrayList<>();

	public void registerXenaLifecycleListeners(Collection<XenaLifecycleListener> listeners) {
		if (Objects.nonNull(listeners) && !listeners.isEmpty()) {
			lifecycleListeners.addAll(listeners);
		}
	}

	public void registerXenaLifecycleListener(XenaLifecycleListener listener) {
		lifecycleListeners.add(listener);
	}

	@Override
	public XenaLifecycleListener[] getXenaLifecycleListeners() {
		return lifecycleListeners.toArray(new XenaLifecycleListener[lifecycleListeners.size()]);
	}

	@Override
	public void removeXenaLifecycleListener(XenaLifecycleListener listener) {
		lifecycleListeners.remove(listener);
	}

	public void publishEvent(Object source, String type, Object data) {
		XenaLifecycleEvent event = new XenaLifecycleEvent(source, type, data);
		lifecycleListeners.forEach(listener -> listener.onEvent(event));
	}

	public void publishEvent(String type, Object data) {
		publishEvent(this, type, data);
	}

}
