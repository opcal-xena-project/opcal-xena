/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.support.scan;

import java.lang.annotation.Annotation;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.util.ClassUtils;

import com.google.common.collect.ImmutableSet;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.xena.core.annotation.EnableXena;
import xyz.opcal.xena.core.annotation.Polymorphism;
import xyz.opcal.xena.core.lifecycle.IStarter;
import xyz.opcal.xena.core.lifecycle.IStoppable;
import xyz.opcal.xena.core.support.Orderable;

@Slf4j
public class XenaScanRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

	private static final Set<String> candidateIndicators = new HashSet<>();
	private static final Set<String> baseInterfaces = new HashSet<>();
	private static final Set<String> basePackages = new LinkedHashSet<>();

	static {
		candidateIndicators.add(Polymorphism.class.getName());
		candidateIndicators.add(EnableXena.class.getName());

		baseInterfaces.add(Orderable.class.getName());
		baseInterfaces.add(IStarter.class.getName());
		baseInterfaces.add(IStoppable.class.getName());

	}

	public static Set<String> getBaseInterfaces() {
		return ImmutableSet.copyOf(baseInterfaces);
	}

	public static boolean isBaseInterface(Class<?> interfaceClass) {
		return baseInterfaces.stream().anyMatch(baseIf -> StringUtils.equals(baseIf, interfaceClass.getName()));
	}

	public static void allBasePackages(Collection<String> pkgs) {
		basePackages.addAll(pkgs);
	}

	public static Set<String> getBasePackages() {
		return ImmutableSet.copyOf(basePackages);
	}

	private final Set<BeanDefinition> xenaBeanDefinitions = new HashSet<>();

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
		// do nothing
	}

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) {
		Set<String> pkgs = new TreeSet<>();
		Arrays.stream(registry.getBeanDefinitionNames()) //
				.map(registry::getBeanDefinition).filter(Objects::nonNull) //
				.forEach(beanDefinition -> loadBeanDefinition(beanDefinition, pkgs));

		pkgs.addAll(getComponentScanningPackages(registry));
		allBasePackages(getRootPackage(pkgs));
		log.info("scan packages [{}]", basePackages);
	}

	private void loadBeanDefinition(BeanDefinition beanDefinition, Set<String> pkgs) {
		String beanClassName = beanDefinition.getBeanClassName();
		if (StringUtils.isBlank(beanClassName)) {
			return;
		}
		try {
			String pkg = ClassUtils.getPackageName(beanClassName);
			if (StringUtils.isNotBlank(pkg) && !StringUtils.startsWithAny(pkg, "org.springframework.", "org.spring")) {
				pkgs.add(pkg);
				loadXenaBean(beanDefinition);
			}
		} catch (BeanDefinitionStoreException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new BeanDefinitionStoreException(MessageFormat.format("Failed to parse configuration class [{0}]", beanClassName), ex);
		}
	}

	private Set<String> getRootPackage(Set<String> pkgs) {
		Set<String> remove = new TreeSet<>();
		Set<String> all = new TreeSet<>(pkgs);
		for (String pkg : pkgs) {
			for (String p : all) {
				if (StringUtils.startsWith(p, pkg) && !StringUtils.equals(p, pkg)) {
					remove.add(p);
				}
			}
		}
		all.removeAll(remove);
		return all;
	}

	protected Set<String> getComponentScanningPackages(BeanDefinitionRegistry registry) {
		Set<String> packages = new LinkedHashSet<>();
		String[] names = registry.getBeanDefinitionNames();
		for (String name : names) {
			BeanDefinition definition = registry.getBeanDefinition(name);
			if (definition instanceof AnnotatedBeanDefinition annotatedDefinition) {
				packages.addAll(addComponentScanningPackages(annotatedDefinition.getMetadata()));
			} else if (definition instanceof AbstractBeanDefinition aBeanDefinition) {
				getBeanClass(aBeanDefinition).ifPresent(c -> packages.addAll(findComponentScanFromClass(c)));
			}
		}
		return packages;
	}

	private Optional<Class<?>> getBeanClass(AbstractBeanDefinition rootBeanDefinition) {
		try {
			return Optional.ofNullable(rootBeanDefinition.getBeanClass());
		} catch (Exception e) {
			// ignore get bean class exception
		}
		return Optional.empty();
	}

	private Set<String> findComponentScanFromClass(Class<?> beanClass) {
		Set<String> packages = new TreeSet<>();
		ComponentScan annotation = beanClass.getAnnotation(ComponentScan.class);
		if (Objects.nonNull(annotation)) {
			addPackages(packages, annotation.value());
			addPackages(packages, annotation.basePackages());
			addClasses(packages, annotation.basePackageClasses());
			if (packages.isEmpty()) {
				packages.add(ClassUtils.getPackageName(beanClass));
			}
		}
		return packages;
	}

	private Set<String> addComponentScanningPackages(AnnotationMetadata metadata) {
		Set<String> packages = new TreeSet<>();
		AnnotationAttributes attributes = AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(ComponentScan.class.getName(), true));
		if (Objects.nonNull(attributes)) {
			addPackages(packages, attributes.getStringArray("value"));
			addPackages(packages, attributes.getStringArray("basePackages"));
			addClasses(packages, attributes.getStringArray("basePackageClasses"));
			if (packages.isEmpty()) {
				packages.add(ClassUtils.getPackageName(metadata.getClassName()));
			}
		}
		return packages;
	}

	private void addClasses(Set<String> packages, Class<?>[] values) {
		if (Objects.nonNull(values)) {
			for (Class<?> value : values) {
				packages.add(ClassUtils.getPackageName(value));
			}
		}
	}

	private void addClasses(Set<String> packages, String[] values) {
		if (Objects.nonNull(values)) {
			for (String value : values) {
				packages.add(ClassUtils.getPackageName(value));
			}
		}
	}

	private void addPackages(Set<String> packages, String[] values) {
		if (Objects.nonNull(values)) {
			Collections.addAll(packages, values);
		}
	}

	private void loadXenaBean(BeanDefinition beanDefinition) {
		if (beanDefinition instanceof AnnotatedBeanDefinition annotatedDefinition && hasAnnotation(annotatedDefinition.getMetadata())) {
			xenaBeanDefinitions.add(beanDefinition);
		} else if (beanDefinition instanceof AbstractBeanDefinition aBeanDefinition) {
			getBeanClass(aBeanDefinition).filter(Objects::nonNull).filter(this::hasAnnotation).ifPresent(c -> xenaBeanDefinitions.add(beanDefinition));
		}
	}

	private boolean hasAnnotation(Class<?> beanClass) {
		for (String xenaAnnotation : candidateIndicators) {
			try {
				Boolean checkResult = Optional.ofNullable(beanClass.getAnnotations()).filter(ArrayUtils::isNotEmpty)
						.map(annotations -> matchAnnotation(annotations, xenaAnnotation)).orElse(false);
				if (Boolean.TRUE.equals(checkResult)) {
					return true;
				}
			} catch (Exception e) {
				// ignore
			}
		}
		return false;
	}

	private boolean matchAnnotation(Annotation[] annotations, String annotationClassName) {
		return Arrays.stream(annotations).map(Annotation::annotationType).filter(Objects::nonNull)
				.anyMatch(c -> StringUtils.equals(c.getName(), annotationClassName));
	}

	private boolean hasAnnotation(AnnotationMetadata metadata) {
		for (String xenaAnnotation : candidateIndicators) {
			if (metadata.hasAnnotation(xenaAnnotation)) {
				return true;
			}
		}
		return false;
	}

}
