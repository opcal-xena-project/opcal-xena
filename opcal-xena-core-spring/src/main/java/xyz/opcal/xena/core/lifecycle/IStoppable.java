/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.lifecycle;

import xyz.opcal.xena.core.support.Orderable;

/**
 * Xena lifecycle interface for destroying.
 * 
 * @see xyz.opcal.xena.core.context.impl.SpringXenaContext#stop()
 */
public interface IStoppable extends Orderable {

	/**
	 * graceful shutdown before {@link xyz.opcal.xena.core.context.impl.SpringXenaContext} destroyed
	 * <br>
	 * 
	 * @see xyz.opcal.xena.core.context.impl.SpringXenaContext#stop()
	 */
	void stop();
}
