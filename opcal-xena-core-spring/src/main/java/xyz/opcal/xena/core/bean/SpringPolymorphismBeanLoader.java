/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.core.bean;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.xena.core.annotation.Polymorphism;
import xyz.opcal.xena.core.context.ApplicationXenaContext;
import xyz.opcal.xena.core.exception.XenaInitializationException;
import xyz.opcal.xena.core.lifecycle.IStarter;
import xyz.opcal.xena.core.support.scan.RuntimeCandidateComponentProvider;
import xyz.opcal.xena.core.support.scan.XenaScanRegistryPostProcessor;
import xyz.opcal.xena.core.support.scan.filter.AnnotationRuntimeTypeFilter;

/**
 * @author gissily
 *
 */
@Slf4j
public class SpringPolymorphismBeanLoader implements PolymorphismBeanLoader, IStarter {

	protected static final String CLASS_PREFIX = "class://";

	protected final Table<String, String, String> beanTable = HashBasedTable.create();

	private ApplicationXenaContext xenaContext;

	/**
	 * @param xenaContext
	 */
	public SpringPolymorphismBeanLoader(ApplicationXenaContext xenaContext) {
		this.xenaContext = xenaContext;
	}

	protected void initLoader() {
		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
		scanner.setResourceLoader(xenaContext.getApplicationContext());
		scanner.addIncludeFilter(new AnnotationTypeFilter(Polymorphism.class));
		XenaScanRegistryPostProcessor.getBasePackages().stream().map(pkg -> findPackage(scanner, pkg)).forEach(this::loadDefinitions);

		RuntimeCandidateComponentProvider runtimeScanner = new RuntimeCandidateComponentProvider(xenaContext.getApplicationContext());
		runtimeScanner.addIncludeFilter(new AnnotationRuntimeTypeFilter(Polymorphism.class));
		runtimeScanner.findCandidateComponents().forEach(this::loadBeans);
	}

	private Set<BeanDefinition> findPackage(ClassPathScanningCandidateComponentProvider scanner, String pkg) {
		try {
			Set<BeanDefinition> components = scanner.findCandidateComponents(pkg);
			log.debug("package [{}] find candidate components [{}]", pkg, components);
			return components;
		} catch (Exception e) {
			log.warn("package {} load error {}", pkg, e.getMessage());
		}
		return new HashSet<>();
	}

	private void loadDefinitions(Set<BeanDefinition> definitions) {
		definitions.stream().forEach(this::loadBeans);
	}

	private void loadBeans(BeanDefinition beanDefinition) {
		try {
			Class<?> clazz = Class.forName(beanDefinition.getBeanClassName());
			Polymorphism annotation = clazz.getAnnotation(Polymorphism.class);
			Class<?>[] interfaceClasses = annotation.interfaceClass();
			String totalInf = Arrays.toString(interfaceClasses);
			for (Class<?> ifclass : interfaceClasses) {
				log.debug("load Polymorphism bean beanDefinition [{}] interfaceClasses [{}]", clazz, totalInf);
				String ifclassName = ifclass.getName();
				String selector = annotation.selector();
				String beanName = annotation.value();
				if (StringUtils.isBlank(beanName)) {
					beanName = CLASS_PREFIX + clazz.getName();
				}
				if (beanTable.contains(ifclassName, selector) && !StringUtils.equals(beanName, beanTable.get(ifclassName, selector))) {
					throw new XenaInitializationException(
							MessageFormat.format("interface [{0}] selector value [{1}] is duplicated, please check the class [{2}] and [{3}]. ", ifclassName,
									selector, clazz.getName(), beanTable.get(ifclassName, selector)));
				}
				log.debug("Polymorphism bean interface [{}] selector [{}] bean name [{}]", ifclassName, selector, beanName);
				beanTable.put(ifclassName, selector, beanName);
			}
		} catch (ClassNotFoundException e) {
			log.warn("class {} not found error {}", beanDefinition.getBeanClassName(), e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	public final <T> T getImplementsInstance(Class<T> interfaceClazz, String selector) {
		if (interfaceClazz == null || StringUtils.isBlank(selector)) {
			return null;
		}
		try {
			String beanName = beanTable.get(interfaceClazz.getName(), selector);
			if (StringUtils.isBlank(beanName)) {
				return null;
			}

			if (StringUtils.startsWith(beanName, CLASS_PREFIX)) {
				return (T) xenaContext.getBean(Class.forName(StringUtils.substringAfter(beanName, CLASS_PREFIX)));
			} else {
				return xenaContext.getBean(beanName, interfaceClazz);
			}

		} catch (Exception e) {
			log.error("get polymorphic instance error: ", e);
		}
		return null;
	}

	@Override
	public int getOrder() {
		return -97;
	}

	@Override
	public void start() {
		initLoader();
	}

}
