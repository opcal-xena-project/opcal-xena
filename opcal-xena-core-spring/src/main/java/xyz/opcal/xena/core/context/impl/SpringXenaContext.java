/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.core.context.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.Phased;
import org.springframework.context.SmartLifecycle;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.core.Ordered;

import lombok.Getter;
import xyz.opcal.xena.core.context.ApplicationXenaContext;
import xyz.opcal.xena.core.exception.XenaInitializationException;
import xyz.opcal.xena.core.lifecycle.IStarter;
import xyz.opcal.xena.core.lifecycle.IStoppable;
import xyz.opcal.xena.core.lifecycle.XenaLifecycleBase;
import xyz.opcal.xena.core.lifecycle.XenaLifecycleConstants;
import xyz.opcal.xena.core.lifecycle.listener.XenaLifecycleEvent;
import xyz.opcal.xena.core.lifecycle.listener.XenaLifecycleListener;

/**
 * @author gissily
 *
 */
public class SpringXenaContext extends XenaLifecycleBase
		implements ApplicationXenaContext, SmartLifecycle, ApplicationContextAware, Phased, ApplicationListener<ApplicationEvent>, Ordered {

	private final ApplicationEventPublisher publisher;
	private ApplicationContext applicationContext;

	private final AtomicBoolean isRunning = new AtomicBoolean(false);

	@Getter
	private final List<IStarter> starterList = new ArrayList<>();

	@Getter
	private final List<IStoppable> stopList = new ArrayList<>();

	@Nullable
	private Thread shutdownHook;

	public SpringXenaContext(ApplicationEventPublisher publisher) {
		this.publisher = publisher;
	}

	protected void registerStarters(Collection<IStarter> starters) {
		if (Objects.nonNull(starters) && !starters.isEmpty()) {
			starters.forEach(this::registerStarter);
		}
	}

	protected void registerStarter(IStarter starter) {
		if (Objects.nonNull(starter)) {
			starterList.add(starter);
		}
	}

	protected void registerStoppables(Collection<IStoppable> stoppable) {
		if (Objects.nonNull(stoppable) && !stoppable.isEmpty()) {
			stoppable.forEach(this::registerStoppable);
		}
	}

	protected void registerStoppable(IStoppable stoppable) {
		if (Objects.nonNull(stoppable)) {
			stopList.add(stoppable);
		}
	}

	protected void stop(List<IStoppable> appList) {
		log.info("Stopping Xena Stoppables");
		for (IStoppable appStoppable : appList) {
			try {
				appStoppable.stop();
			} catch (Exception e) {
				log.error("xena stop error: ", e);
			}
		}
	}

	protected void start(List<IStarter> appList) {
		log.info("Starting Xena Starters");
		for (IStarter appStarter : appList) {
			try {
				log.debug("appStarter [{}] starting", appStarter.getClass());
				appStarter.start();
			} catch (Exception e) {
				throw new XenaInitializationException("xena start error: ", e);
			}
		}
	}

	@Override
	public void start() {
		if (!isRunning.getAndSet(true)) {
			registerStarters(getBeans(IStarter.class));
			Collections.sort(starterList);

			registerStoppables(getBeans(IStoppable.class));
			Collections.sort(stopList);

			registerXenaLifecycleListeners(getBeans(XenaLifecycleListener.class));

			publishEvent(XenaLifecycleConstants.CONTEXT_START, null);

			start(starterList);

			registerXenaLifecycleListener(event -> {
				if (StringUtils.equals(XenaLifecycleConstants.SHUTDOWN, event.getType())) {
					log.info("Shutdown from XLifecycle event");
					stop();
				}
			});

			registerShutdownHook();
		}
	}

	public void registerShutdownHook() {
		if (Objects.isNull(shutdownHook)) {
			this.shutdownHook = new Thread() {
				@Override
				public void run() {
					synchronized (this) {
						publishEvent(XenaLifecycleConstants.SHUTDOWN_HOOK_STOP, null);
						SpringXenaContext.this.stop();
					}
				}
			};
			Runtime.getRuntime().addShutdownHook(this.shutdownHook);
		}
	}

	@Override
	public void stop() {
		if (isRunning.getAndSet(false)) {
			log.info("XenaContext will be shutdown");

			publishEvent(XenaLifecycleConstants.CONTEXT_STOP, null);

			stop(stopList);
		}
	}

	public void contextDestroy() {
		publishEvent(XenaLifecycleConstants.SHUTDOWN, null);
		if (Objects.nonNull(shutdownHook)) {
			try {
				Runtime.getRuntime().removeShutdownHook(this.shutdownHook);
			} catch (IllegalStateException ex) {
				// ignore - VM is already shutting down
			}
		}
	}

	@Override
	public boolean isRunning() {
		return isRunning.get();
	}

	@Override
	public int getOrder() {
		return LOWEST_PRECEDENCE;
	}

	@Override
	public void publishEvent(Object source, String type, Object data) {
		super.publishEvent(source, type, data);
		publisher.publishEvent(new XenaLifecycleEvent(source, type, data));
	}

	@Override
	public void onApplicationEvent(ApplicationEvent event) {
		if (event instanceof ContextClosedEvent && isRunning()) {
			try {
				log.info("XenaContext will be shutdown from ContextClosedEvent");
				publishEvent(XenaLifecycleConstants.SHUTDOWN, null);
			} catch (Exception e) {
				log.error("shutdown error", e);
			}
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	@Override
	public Object getBean(String beanName) {
		return applicationContext.getBean(beanName);
	}

	@Override
	public <T> T getBean(Class<T> beanClass) {
		return applicationContext.getBean(beanClass);
	}

	@Override
	public <T> T getBean(String beanName, Class<T> beanClass) {
		return applicationContext.getBean(beanName, beanClass);
	}

	@Override
	public <T> Set<T> getBeans(Class<T> beanClass) {
		Map<String, T> beansOfType = applicationContext.getBeansOfType(beanClass);
		return new LinkedHashSet<>(beansOfType.values());
	}

}
