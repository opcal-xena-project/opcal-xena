/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.lifecycle.listener;

import java.util.EventObject;

import lombok.Getter;

/**
 * Xena lifecycle event object.
 * 
 * @author gissily
 *
 */
@Getter
public class XenaLifecycleEvent extends EventObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6922564126062328522L;

	private String type;
	private transient Object data;

	public XenaLifecycleEvent(Object source, String type, Object data) {
		super(source);
		this.type = type;
		this.data = data;
	}

}
