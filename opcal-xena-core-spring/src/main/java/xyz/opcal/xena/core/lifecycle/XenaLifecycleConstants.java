/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.lifecycle;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class XenaLifecycleConstants {

	public static final String CONTEXT_START = "context_start";
	public static final String CONTEXT_STOP = "context_stop";
	public static final String STARTER_BEFORE = "starter_before";
	public static final String STARTER_AFTER = "starter_after";
	public static final String STOPPABLE_BEFORE = "stoppable_before";
	public static final String STOPPABLE_AFTER = "stoppable_after";
	public static final String SHUTDOWN_HOOK_STOP = "shutdown_hook_stop";

	public static final String SHUTDOWN = "shutdown";
}
