/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.core.configutation;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import xyz.opcal.xena.core.bean.PolymorphismBeanLoader;
import xyz.opcal.xena.core.bean.SpringPolymorphismBeanLoader;
import xyz.opcal.xena.core.context.XenaContext;
import xyz.opcal.xena.core.context.impl.SpringXenaContext;
import xyz.opcal.xena.core.support.scan.XenaScanRegistryPostProcessor;

/**
 * @author gissily
 *
 */
@ComponentScan("xyz.opcal.xena")
public class XenaCoreConfiguration {

	@Bean
	public XenaScanRegistryPostProcessor xenaScanRegistryPostProcessor() {
		return new XenaScanRegistryPostProcessor();
	}

	@Bean
	public XenaContext xenaContext(ApplicationEventPublisher publisher) {
		return new SpringXenaContext(publisher);
	}

	@Bean
	public PolymorphismBeanLoader polymorphismBeanLoader(XenaContext xenaContext) {
		return new SpringPolymorphismBeanLoader((SpringXenaContext) xenaContext);
	}
}
