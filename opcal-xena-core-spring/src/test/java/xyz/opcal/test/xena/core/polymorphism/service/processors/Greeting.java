package xyz.opcal.test.xena.core.polymorphism.service.processors;

public interface Greeting {

	String sayHello();
}
