package xyz.opcal.test.xena.core.polymorphism.load;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContextException;

import xyz.opcal.xena.core.exception.XenaInitializationException;

class PolymorphismLoadTests {

	@Test
	void testLoadBeanError() {
		ApplicationContextException exception = assertThrows(ApplicationContextException.class,
				() -> SpringApplication.run(PolymorphismLoadConfiguration.class, new String[] {}));
		assertNotNull(exception.getCause());
		assertEquals(XenaInitializationException.class, exception.getCause().getClass());
	}

}
