package xyz.opcal.test.xena.core.lifecycle.listener;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import xyz.opcal.xena.core.annotation.EnableXena;

@Configuration
@ComponentScan
@EnableXena
public class ListenerConfiguration {

}
