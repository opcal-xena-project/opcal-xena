package xyz.opcal.test.xena.core.polymorphism.service.processors.impl;

import xyz.opcal.test.xena.core.polymorphism.service.processors.Greeting;
import xyz.opcal.xena.core.annotation.Polymorphism;

@Polymorphism(value = "InFr", selector = "fr", interfaceClass = Greeting.class)
public class FrGreeting implements Greeting {

	@Override
	public String sayHello() {
		return "Bonjour";
	}

}
