package xyz.opcal.test.xena.core.polymorphism.load;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import xyz.opcal.xena.core.annotation.EnableXena;

@Configuration
@ComponentScan(value = {"xyz.opcal.test.xena.core.polymorphism"})
@EnableXena
public class PolymorphismLoadConfiguration {

}
