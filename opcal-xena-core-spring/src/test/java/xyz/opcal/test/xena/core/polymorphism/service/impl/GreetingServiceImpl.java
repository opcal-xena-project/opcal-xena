/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.test.xena.core.polymorphism.service.impl;

import org.springframework.stereotype.Service;

import xyz.opcal.test.xena.core.polymorphism.service.GreetingService;
import xyz.opcal.test.xena.core.polymorphism.service.processors.Greeting;
import xyz.opcal.xena.core.bean.PolymorphismBeanLoader;

/**
 * @author gissily
 *
 */
@Service
public class GreetingServiceImpl implements GreetingService {

	private PolymorphismBeanLoader polymorphismBeanLoader;

	public GreetingServiceImpl(PolymorphismBeanLoader polymorphismBeanLoader) {
		this.polymorphismBeanLoader = polymorphismBeanLoader;
	}

	public String sayHello(String lang) {
		Greeting greeting = polymorphismBeanLoader.getImplementsInstance(Greeting.class, lang);
		if (greeting == null) {
			throw new IllegalArgumentException("not support greet in lanaguage " + lang);
		}
		return greeting.sayHello();
	}

}
