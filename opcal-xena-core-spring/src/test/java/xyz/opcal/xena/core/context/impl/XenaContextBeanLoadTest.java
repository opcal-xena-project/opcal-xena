/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.core.context.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import xyz.opcal.test.xena.core.polymorphism.service.PolymorphismServiceConfiguration;
import xyz.opcal.test.xena.core.polymorphism.service.processors.Greeting;
import xyz.opcal.xena.core.bean.PolymorphismBeanLoader;
import xyz.opcal.xena.core.context.XenaContext;

/**
 * @author gissily
 *
 */
@EnableAutoConfiguration
@SpringJUnitConfig(classes = { PolymorphismServiceConfiguration.class })
class XenaContextBeanLoadTest {

	private @Autowired XenaContext xenaContext;

	@Test
	void load() {
		assertNotNull(xenaContext.getBean(PolymorphismBeanLoader.class));
		assertNotNull(xenaContext.getBean("InFr"));
		assertNotNull(xenaContext.getBean("InFr", Greeting.class));
		assertNotNull(xenaContext.getBeans(Greeting.class));
	}
}
