/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.context.impl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.xena.core.DefaultTestConfiguration;

@Slf4j
@SpringJUnitConfig(classes = { DefaultTestConfiguration.class })
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class XenaContextShutdownTest {

	private @Autowired ApplicationEventPublisher publisher;
	private SpringXenaContext xenaContext;

	@BeforeEach
	void init() {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		applicationContext.scan("xyz.opcal.xena");
		applicationContext.refresh();
		xenaContext = new SpringXenaContext(publisher);
		xenaContext.setApplicationContext(applicationContext);
	}

	@Test
	@Order(Integer.MAX_VALUE)
	void shutdownTest() {
		xenaContext.start();
		xenaContext.registerShutdownHook();
		assertDoesNotThrow(() -> xenaContext.start());
		assertDoesNotThrow(() -> xenaContext.onApplicationEvent(new ContextClosedEvent(xenaContext.getApplicationContext())));
		assertDoesNotThrow(() -> xenaContext.contextDestroy());
		assertDoesNotThrow(() -> xenaContext.stop(() -> log.info("stop")));
		xenaContext.registerStarters(null);
		xenaContext.registerStarters(Collections.emptyList());
		xenaContext.stop();
	}

}
