/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.polymorphism;

import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import xyz.opcal.test.xena.core.polymorphism.service.GreetingService;
import xyz.opcal.test.xena.core.polymorphism.service.PolymorphismServiceConfiguration;
import xyz.opcal.test.xena.core.polymorphism.service.processors.Greeting;
import xyz.opcal.xena.core.bean.PolymorphismBeanLoader;

@EnableAutoConfiguration
@SpringJUnitConfig(classes = { PolymorphismServiceConfiguration.class })
class PolymorphismBeanTests {

	private @Autowired GreetingService greetingService;
	private @Autowired PolymorphismBeanLoader polymorphismBeanLoader;

	@Test
	void greetingInDe() {
		String result = greetingService.sayHello("de");
		assertEquals("Hallo", result);
	}

	@Test
	void greetingInEn() {
		String result = greetingService.sayHello("en");
		assertEquals("hello", result);
	}

	@Test
	void greetingInFr() {
		String result = greetingService.sayHello("fr");
		assertEquals("Bonjour", result);
	}

	@Test
	void notSupportLang() {
		assertThatIllegalArgumentException().isThrownBy(() -> greetingService.sayHello("jp"));

		Greeting blankValueInstance = polymorphismBeanLoader.getImplementsInstance(Greeting.class, "   ");
		assertNull(blankValueInstance);
		assertNull(polymorphismBeanLoader.getImplementsInstance(null, "de"));
	}
}
