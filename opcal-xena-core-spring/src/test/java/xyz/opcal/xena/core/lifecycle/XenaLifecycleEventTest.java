/**
 * Copyright 2020-2022 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.opcal.xena.core.lifecycle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import xyz.opcal.test.xena.core.lifecycle.listener.ContextStartListener;
import xyz.opcal.test.xena.core.lifecycle.listener.ListenerConfiguration;
import xyz.opcal.test.xena.core.lifecycle.listener.SpringContextStartListener;
import xyz.opcal.xena.core.context.impl.SpringXenaContext;

/**
 * @author gissily
 *
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringJUnitConfig(classes = ListenerConfiguration.class)
class XenaLifecycleEventTest {

	private @Autowired SpringXenaContext xenaContext;
	private @Autowired ContextStartListener contextStartListener;
	private @Autowired SpringContextStartListener springContextStartListener;

	@Test
	@Order(1)
	void contextStartEvent() {
		assertTrue(contextStartListener.isStarted());
		assertTrue(springContextStartListener.isStarted());
	}

	@Test
	@Order(2)
	void customEvent() {
		String eventType = "customEventTest";
		xenaContext.registerXenaLifecycleListener(event -> {
			if (StringUtils.equals(event.getType(), eventType)) {
				assertEquals(XenaLifecycleEventTest.this, event.getData());
			}
		});
		xenaContext.publishEvent(eventType, this);
	}

	@Test
	@Order(3)
	void contextStopEvent() {
		xenaContext.registerXenaLifecycleListener(event -> {
			if (StringUtils.equals(event.getType(), XenaLifecycleConstants.SHUTDOWN)) {
				assertNull(event.getData());
			}
		});
		xenaContext.publishEvent(XenaLifecycleConstants.SHUTDOWN, null);
	}

	@Test
	@Order(4)
	void eventTest() {
		xenaContext.registerXenaLifecycleListener(null);
		xenaContext.registerXenaLifecycleListeners(null);
		xenaContext.registerXenaLifecycleListeners(Collections.emptyList());
		assertTrue(ArrayUtils.isNotEmpty(xenaContext.getXenaLifecycleListeners()));
		xenaContext.removeXenaLifecycleListener(null);
		xenaContext.removeXenaLifecycleListener(contextStartListener);
	}
}
