/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.context.impl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.xena.core.DefaultTestConfiguration;
import xyz.opcal.xena.core.exception.XenaInitializationException;
import xyz.opcal.xena.core.lifecycle.IStarter;
import xyz.opcal.xena.core.lifecycle.IStoppable;

@Slf4j
@SpringJUnitConfig(classes = { DefaultTestConfiguration.class })
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class XenaContextTest {

	private @Autowired SpringXenaContext xenaContext;

	@Test
	@Order(1)
	void starterExceptionTest() {

		assertNotNull(xenaContext.getApplicationContext());

		List<IStarter> starterList = Arrays.asList(new IStarter() {

			@Override
			public int getOrder() {
				return 0;
			}

			@Override
			public void start() {
				throw new IllegalStateException("start state exception");
			}
		});
		assertThrows(XenaInitializationException.class, () -> xenaContext.start(starterList));
		xenaContext.getStarterList();
		List<IStoppable> stopList = xenaContext.getStopList();
		assertDoesNotThrow(() -> xenaContext.stop(stopList));
	}

	@Test
	@Order(2)
	void initRegisterNullTest() {
		assertDoesNotThrow(() -> xenaContext.registerStarters(null));
		assertDoesNotThrow(() -> xenaContext.registerStoppables(null));
	}

	@Test
	@Order(3)
	void initRegisterTest() {
		List<IStarter> starters = Arrays.asList(new IStarter() {

			@Override
			public int getOrder() {
				return 0;
			}

			@Override
			public void start() {
				log.info("start init");
			}
		}, null);
		assertDoesNotThrow(() -> xenaContext.registerStarters(starters));

		List<IStoppable> stopList = Arrays.asList(new IStoppable() {

			@Override
			public int getOrder() {
				return 0;
			}

			@Override
			public void stop() {
				log.info("stop");
			}
		}, null);

		assertDoesNotThrow(() -> xenaContext.registerStoppables(stopList));
		xenaContext.stop(xenaContext.getStopList());
	}

	@Test
	@Order(4)
	void stopExceptionTest() {
		List<IStoppable> stopList = Arrays.asList(new IStoppable() {

			@Override
			public int getOrder() {
				return 0;
			}

			@Override
			public void stop() {
				throw new IllegalStateException("stop state exception");
			}
		});

		assertDoesNotThrow(() -> xenaContext.stop(stopList));
	}

}
