/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.support;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import lombok.extern.slf4j.Slf4j;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
class OrderableTest {

	@Test
	void test() {

		List<Orderable> list = new ArrayList<>();
		list.add(new Orderable() {

			@Override
			public int getOrder() {
				return 11;
			}
		});
		list.add(new Orderable() {

			@Override
			public int getOrder() {
				return -2;
			}
		});
		list.add(new Orderable() {

			@Override
			public int getOrder() {
				return 90;
			}
		});

		Collections.sort(list);
		StringBuilder builder = new StringBuilder();
		list.forEach(orderable -> {
			if (orderable == null) {
				builder.append("null");
			} else {
				builder.append(orderable.getOrder());
			}
			builder.append(",");
		});
		log.info("sort result is {}", builder.toString());

		assertEquals(-2, list.get(0).getOrder());
	}
}
