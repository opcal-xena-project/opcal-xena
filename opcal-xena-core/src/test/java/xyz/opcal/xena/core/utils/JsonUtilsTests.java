/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.utils;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
class JsonUtilsTests {

	private String jsonData;
	private File dataFile;

	@BeforeAll
	void before() throws Exception {
		Map<String, Object> data = new HashMap<>();
		data.put("name", "jhon");
		data.put("age", 10);
		data.put("code", 900);
		JsonUtils.toJson(data);
		jsonData = JsonUtils.toPrettyJson(data);
		assertNotNull(jsonData);
		dataFile = FileUtils.getFile(".", "testFile" + System.currentTimeMillis() + ".json");
		FileUtils.write(dataFile, jsonData, Charset.defaultCharset(), false);
	}

	@AfterAll
	void after() {
		FileUtils.deleteQuietly(dataFile);
	}

	@Test
	void parseTest() throws Exception {
		assertNotNull(JsonUtils.<Map<String, Object>>readJsonToMap(jsonData));
		assertNotNull(JsonUtils.<Map<String, Object>>readJsonToMap(jsonData.getBytes()));
		try (InputStream is = new ByteArrayInputStream(jsonData.getBytes());
				Reader reader = new InputStreamReader(new ByteArrayInputStream(jsonData.getBytes()))) {
			assertNotNull(JsonUtils.<Map<String, Object>>readJsonToMap(is));
			assertNotNull(JsonUtils.<Map<String, Object>>readJsonToMap(reader));
		}

		assertNotNull(JsonUtils.<Map<String, Object>>readJsonToMap(dataFile));
		assertNotNull(JsonUtils.<Map<String, Object>>readJsonToMap(dataFile.toURI().toURL()));
	}
}
