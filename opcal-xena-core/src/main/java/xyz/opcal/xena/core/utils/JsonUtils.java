/**
 * Copyright 2020-2021 Opcal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.opcal.xena.core.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@UtilityClass
public class JsonUtils {

	public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	public static ObjectMapper getDefaultMapper() {
		return OBJECT_MAPPER;
	}

	public static String toJson(Object object) {

		try {
			return OBJECT_MAPPER.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			log.warn("object {} to json string error: ", object, e);
		}
		return null;
	}

	public static String toPrettyJson(Object object) {

		try {
			return OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(object);
		} catch (JsonProcessingException e) {
			log.warn("object {} to pretty json string error: :  ", object, e);
		}
		return null;
	}

	public static <T> T readJsonToMap(String json) throws IOException {
		return OBJECT_MAPPER.readValue(json, new JsonTypeReference<T>());
	}

	public static <T> T readJsonToMap(InputStream src) throws IOException {
		return OBJECT_MAPPER.readValue(src, new JsonTypeReference<T>());
	}

	public static <T> T readJsonToMap(File src) throws IOException {
		return OBJECT_MAPPER.readValue(src, new JsonTypeReference<T>());
	}

	public static <T> T readJsonToMap(byte[] src) throws IOException {
		return OBJECT_MAPPER.readValue(src, new JsonTypeReference<T>());
	}

	public static <T> T readJsonToMap(URL src) throws IOException {
		return OBJECT_MAPPER.readValue(src, new JsonTypeReference<T>());
	}

	public static <T> T readJsonToMap(Reader src) throws IOException {
		return OBJECT_MAPPER.readValue(src, new JsonTypeReference<T>());
	}

	static class JsonTypeReference<T> extends TypeReference<T> {

	}
}
